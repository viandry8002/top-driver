import React, { Component } from 'react'
import { StyleSheet, Text, View, SafeAreaView} from 'react-native'
import { List } from 'react-native-paper';
import { Button  } from 'react-native-elements';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Entypo from 'react-native-vector-icons/Entypo';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import colors from '../../Style/Colors';
import styleses from '../../Style/Styleses';


const Callc = () => {

    const [expanded, setExpanded] = React.useState(true);

  const handlePress = () => setExpanded(!expanded);
  
    return (
    <SafeAreaView style={[styleses.wrap,{backgroundColor:colors.white}]}>
        
        <View style={{margin:20}}>
        {/* Gudang */}
        <View style={styles.containlist}>
        <List.Accordion
            title="Gudang"
            left={() => ( 
            <View style={styles.listtop}>
                <Entypo name="home" size={30} color={colors.darkblue}/>
            </View>
            )
            }>
            <View style={styles.listAccor}>
                <Text style={{marginTop:10}}>0812 3456 7890</Text>
                <Button title='Hubungi' buttonStyle={{backgroundColor:colors.darkblue,width:100}}  />
            </View>
        </List.Accordion>
        </View>
        {/* Penerima */}
        <View style={styles.containlist}>
        <List.Accordion
            title="Penerima"
            left={() => ( 
                <View style={styles.listtop}>
                    <Ionicons name="call" size={30} color={colors.darkblue}/>
                </View>
                )
                }>
            <View style={styles.listAccor}>
                <Text style={{marginTop:10}}>0812 3456 7890</Text>
                <Button title='Hubungi' buttonStyle={{backgroundColor:colors.darkblue,width:100}}  />
            </View>
        </List.Accordion>
        </View>
         {/* RS */}
         <View style={styles.containlist}>
        <List.Accordion
            title="Rumah Sakit Terdekat"
            left={() => ( 
                <View style={styles.listtop}>
                    <MaterialCommunityIcons name="hospital" size={35} color={colors.darkblue}/>
                </View>
                )
                }>
            <View style={styles.listAccor}>
                <Text style={{marginTop:10}}>0812 3456 7890</Text>
                <Button title='Hubungi' buttonStyle={{backgroundColor:colors.darkblue,width:100}}  />
            </View>
        </List.Accordion>
        </View>
         {/* Polisi */}
         <View style={styles.containlist}>
        <List.Accordion
            title="Kepolisian Terdekat"
            left={() => ( 
                <View style={styles.listtop}>
                    <MaterialCommunityIcons name="police-badge" size={30} color={colors.darkblue}/>
                </View>
                )
                }>
            <View style={styles.listAccor}>
                <Text style={{marginTop:10}}>0812 3456 7890</Text>
                <Button title='Hubungi' buttonStyle={{backgroundColor:colors.darkblue,width:100}}  />
            </View>
        </List.Accordion>
        </View>
        </View>
    </SafeAreaView>
    )
}

export default Callc

const styles = StyleSheet.create({
    containlist : {
        backgroundColor:colors.white,
        borderRadius:10,
        marginVertical:10,
        elevation: 10,
        zIndex: 1
    },
    listtop : {
        width:50,
        height:50,
        borderRadius:50,
        backgroundColor:'rgba(27, 91, 202, 0.1)',
        alignItems:'center',
        padding:8
    },
    listAccor : {
        margin:10,
        justifyContent:'space-between',
        flexDirection:'row'
    }
})
