import React, { useState, useEffect} from 'react'
import { StyleSheet, Text, View,StatusBar,Image,TouchableOpacity,SafeAreaView,LogBox } from 'react-native'
// import { Button } from 'react-native-elements';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
// import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

import  SplashScreens  from './Splashscreen';
import colors from '../Style/Colors';

import Login from './Login';
// Home
import Home from './Home';
import Profile from './Profile';
import Detail from './Home/Detail';
import Maps from './Home/Maps';
import Terima from './Home/Terima';
import Callc from './Home/Callc';
// Absensi
import Absensi from './Absensi';
// Tugas
import Pending from './Tugas/Pending';
import Proses from './Tugas/Proses';
import Selesai from './Tugas/Selesai';
// Kendaraan
import Kendaraan from './Kendaraan';
// Ritase
import Hari from './Ritase/Hari';
import Minggu from './Ritase/Minggu';
import Bulan from './Ritase/Bulan';
import DtlRitase from './Ritase/DtlRitase';

import Fontisto from 'react-native-vector-icons/Fontisto';


const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();
const Toptab = createMaterialTopTabNavigator();

  const Header = () => (
    {headerShown: false}
  )

  // const HeaderRitase = (title,number) => (
  //   <View style={{flexDirection:'row'}}>
  //     <Text style={{margin:10,fontSize:15}}>{title}</Text>
  //     <View style={{width:20,height:20,borderRadius:20,backgroundColor:'red',marginTop:10,alignItems:'center'}}>
  //     <Text style={{color:colors.white}}>{number}</Text>
  //     </View>
  //     </View>
  // )

  const Homes = () => {
    return (
      <Stack.Navigator>
      <Stack.Screen name="Home" 
      component={Home}
      options={() => Header()}
      />
      </Stack.Navigator>
    );
  }

  // Absensi
  const BotomAbsensi= () => {
    return (
      <Stack.Navigator initialRouteName={'Absensi'}>
       <Stack.Screen name="Absensi" component={Absensi}
           options={{
             title:'Absensi',
             headerLeft: false,
             headerTitleAlign:'center'
            }}
         />
      </Stack.Navigator>
    );
  }
  // Absensi
  // Tugas 
  const TopTugas= () => {
    return (
    <Toptab.Navigator>
      <Toptab.Screen name="Pending" component={Pending} options={{title:'Pending'}}/>
      <Toptab.Screen name="Proses" component={Proses} options={{title:'Sedang Proses'}}/>
      <Toptab.Screen name="Selesai" component={Selesai} options={{title:'Selesai'}}/>
    </Toptab.Navigator>
    )
  }

  const BotomTugas= () => {
    return (
      <Stack.Navigator initialRouteName={'TopTugas'}>
       <Stack.Screen name="TopTugas" component={TopTugas}
           options={{
             title:'Tugas',
             headerLeft: false,
             headerTitleAlign:'center'
            }}
         />
      </Stack.Navigator>
    );
  }
  // Tugas
  // Kendaraan 
  const Botomkendaraan= () => {
    return (
      <Stack.Navigator initialRouteName={'Kendaraan'}>
       <Stack.Screen name="Kendaraan" component={Kendaraan}
           options={{
             title:'Cek Kendaraan',
             headerLeft: false,
             headerTitleAlign:'center'
            }}
         />
      </Stack.Navigator>
    );
  }
  // Kendaraan
  // Ritase 
  const TopRitase= () => {
    return (
    <Toptab.Navigator>
      <Toptab.Screen name="Hari" component={Hari} options={{title: 'Hari ini'}}/>
      <Toptab.Screen name="Minggu" component={Minggu} options={{title: 'Minggu ini'}}/>
      <Toptab.Screen name="Bulan" component={Bulan} options={{title: 'Bulan ini',}}/>
    </Toptab.Navigator>
    )
  }

  const BotomRitase= () => {
    return (
      <Stack.Navigator initialRouteName={'TopRitase'}>
       <Stack.Screen name="TopRitase" component={TopRitase}
           options={{
             title:'Daftar Ritase',
             headerLeft: false,
             headerTitleAlign:'center'
            }}
         />
      </Stack.Navigator>
    );
  }
  // Ritase


  const MainTabNavigation = () => {
    return(
          <Tab.Navigator
          nitialRouteName={'Home'}
          screenOptions={({ route }) => ({
            tabBarIcon: ({ focused, color, size }) => {
              let iconName;

              if (route.name === 'Home') {
                iconName = focused
                  ? 'home'
                  : 'home';
              } 
              else if (route.name === 'Absensi') {
                iconName = focused ? 'calendar-alt' : 'calendar-alt';
              }else if (route.name === 'Tugas') {
                iconName = focused ? 'tasks' : 'tasks';
              }else if (route.name === 'Kendaraan') {
                iconName = focused ? 'truck' : 'truck';
              }else if (route.name === 'Ritase') {
                iconName = focused ? 'clipboard' : 'clipboard';
               }

              // You can return any component that you like here!
              return <FontAwesome5 name={iconName} size={size} color={color} />;
            },
          })}
          tabBarOptions={{
            activeTintColor: colors.darkblue,
            inactiveTintColor: 'gray',
          }}
        >
          <Tab.Screen name="Home" component={Homes} />
          <Tab.Screen name="Absensi" component={BotomAbsensi} options={{title:'Absensi'}}/>
          <Tab.Screen name="Tugas" component={BotomTugas} options={{title:'Tugas'}}/>
          <Tab.Screen name="Kendaraan" component={Botomkendaraan} options={{title:'Cek Kendaraan'}}/>
          <Tab.Screen name="Ritase" component={BotomRitase} options={{title:'Daftar Ritase'}}/>
        </Tab.Navigator>
    )
  }
  
    const MainNavigation = () => (
      //  <Stack.Navigator initialRouteName={props.token !== null ? 'Home' : 'Intro'}> 
       <Stack.Navigator initialRouteName={'Login'}>
       <Stack.Screen name="Login" component={Login}
           options={Header()}
         />
      <Stack.Screen name="Home" component={MainTabNavigation}
           options={Header()}
         />
      <Stack.Screen name="Profile" component={Profile}
           options={Header()}
         />

      <Stack.Screen name="Detail" component={Detail}
           options={{
             title:'Detail Pengiriman',
             headerTitleAlign:'center'
            }}
         />
      <Stack.Screen name="Maps" 
          component={Maps}
          options={() => Header()}
          />
      <Stack.Screen name="Terima" component={Terima}
           options={{
             title:'Paket Di Terima',
             headerTitleAlign:'center'
            }}
         />

      <Stack.Screen name="DtlRitase" component={DtlRitase}
           options={{
             title:'Detail Ritase',
             headerTitleAlign:'center'
            }}
         />

      <Stack.Screen name="Callc" component={Callc}
           options={{
             title:'Call Center',
             headerTitleAlign:'center'
            }}
         />
      </Stack.Navigator>
  )

const Routes = () => {
    const [isLoading, setIsLoading] = useState(true)
    const [token, setToken] = useState(null)

    useEffect(() => {
        setTimeout(() => {
            setIsLoading(!isLoading)
        }, 3000)

        // async function getToken(){
        //   const token = await AsyncStorage.getItem('api_token')
        //   setToken(token)
        // }
        // getToken()

    },[])

    if(isLoading) {
        return <SplashScreens/>
    }

    return (
      <NavigationContainer>
        <MainNavigation/>
          {/* <MainNavigation token={token}/> */}
      </NavigationContainer>
  )
}

export default Routes

const styles = StyleSheet.create({
    iconContainer: {
    flexDirection: "row",
    justifyContent: "space-evenly",
    width: 120
  }
})
