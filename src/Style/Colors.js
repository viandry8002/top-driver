const colors = {
    white : '#FFFFFF',
    blue: '#3598DB',
    darkblue: '#1B5BCA',
    black: '#000000',
    grey: '#DCDCDC',
    lightGrey: '#e6e6e6',
    transparent: 'rgba(0, 0, 0, 0.5)',
    green: '#24B13B',
    red:'#D52222'
}

export default colors